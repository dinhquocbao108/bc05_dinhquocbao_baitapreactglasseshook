import React from 'react'

export default function Detail({detail}) {
  return (
    <div className='mt-5 text-center bg-white'>
      <h1>{detail.name}</h1>
      <span className='display-3'>{detail.price}$</span>
      <p>{detail.desc}</p>
    </div>
  )
}
