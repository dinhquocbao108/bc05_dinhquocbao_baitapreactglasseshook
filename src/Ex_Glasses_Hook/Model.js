import React,{useEffect, useState} from 'react'
import GlassesList from './GlassesList';
import Detail from './Detail'
import { dataGlasses } from './dataGlasses';


export default function Model() {
    let [detail,setDetail] = useState(dataGlasses[0]);
    let [flag,setFlag] = useState(true);
    let data = {};
    let handleChangeDetail =(glasse)=>{
      setDetail(detail=glasse)
    };
   let removeGlasse = (param) =>{
    if(param===false){
      setFlag(flag=false);
   };
   if(param===true){
    setFlag(flag=true);
   }

  };
  return (
    <div>
        <div className='row '>
            <div className='col-5 mr-5'>
            <GlassesList handleChangeDetail={handleChangeDetail}/>
            </div>
            <div className='col-6 vglasses__model'>
            <button onClick={()=>{
              removeGlasse(false);
            }} className='btn btn-warning'>before</button>
            <button onClick={()=>{
              removeGlasse(true);
            }} className='btn btn-success ml-2'>after</button>
            {flag?<img  src={detail.url} alt="" />:<img  style={{display:"none"}} src={detail.url} alt="" />}
            </div>

        </div>
            <Detail detail={detail}/>
    </div>

  )
}
