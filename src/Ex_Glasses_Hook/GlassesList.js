import React,{ useState } from 'react'
import ItemGlasses from './ItemGlasses';
import { dataGlasses } from './dataGlasses';



export default function GlassesList({handleChangeDetail}) {
  const [glassesArr,setDataGlasses] = useState(dataGlasses);
   let  renderGlassesList = ()=>{
    return glassesArr.map((item)=>{
        return <ItemGlasses handleChangeDetail={handleChangeDetail}  glasses={item}/>
    })
   }
  return (
    <div className='row bg-white mt-5'>{renderGlassesList()}</div>
  )
}
