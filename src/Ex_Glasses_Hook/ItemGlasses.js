import React from 'react'

export default function ItemGlasses({glasses,handleChangeDetail}) {
 
  return (
    <div className='col-4 mb-5'>
        <div onClick={()=>{
          handleChangeDetail(glasses)
        }}>
            <img className='imgGlasses mt-5'  src={glasses.url} alt="" />
        </div>

    </div>
  )
}
